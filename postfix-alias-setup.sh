#!/bin/bash          
rm /var/spool/postfix/pid/* || true 

apk update 
#apk del postfix-pcre
apk add --no-cache cyrus-sasl cyrus-sasl-crammd5 cyrus-sasl-digestmd5 cyrus-sasl-login cyrus-sasl-ntlm postfix curl pcre2 bash postfix-pcre ca-certificates

postconf -e mailbox_size_limit=512000000
postconf -p message_size_limit|grep message_size_limit|grep -e =$ -e "= 0$" && postconf -e message_size_limit=512000000



postconf -e "mydomain=$MAILNAME"

postconf -e "enable_original_recipient = yes"
test -e /etc/postfix/generic_transport_map || touch /etc/postfix/generic_transport_map

test -e /etc_postfix_sasl_passwd && ln -sf /etc_postfix_sasl_passwd /etc/postfix/sasl_passwd
chmod go-w /etc/postfix/sasl_passwd

echo "$BOUNCE_MAIL_ADDRESS"|grep "nobody@this-user-did-not-configure-his-bounce-adress.lan" -q ||  {
touch /etc/aliases
 for aliasuser in root www-data mail postmaster abuse noc admin webmaster mailer-daemon;do
 grep "^$aliasuser:" /etc/aliases -q && echo "NOT MODIFYING $aliasuser alias"
 grep "^$aliasuser:" /etc/aliases -q || (echo "$aliasuser:    $BOUNCE_MAIL_ADDRESS" >> /etc/aliases  )
 #grep "^$aliasuser@${MAILNAME}:" /etc/aliases -q || (echo "$aliasuser@${MAILNAME}:    $BOUNCE_MAIL_ADDRESS" >> /etc/aliases  )
 [[ -z "$FORCED_SENDING_DOMAIN" ]] || [[  "$MAILNAME" = "$FORCED_SENDING_DOMAIN" ]] ||  {

     #grep "^$aliasuser@$FORCED_SENDING_DOMAIN:" /etc/aliases -q && echo "NOT MODIFYING $aliasuser alias"
     #grep "^$aliasuser@$FORCED_SENDING_DOMAIN:" /etc/aliases -q || (echo "$aliasuser@$FORCED_SENDING_DOMAIN:    $BOUNCE_MAIL_ADDRESS" >> /etc/aliases  )
     grep "^/$aliasuser@$FORCED_SENDING_DOMAIN/" /etc/postfix/generic_transport_map -q || echo "NOT MODIFYING $aliasuser transport_map"
     grep "^/$aliasuser@$FORCED_SENDING_DOMAIN/" /etc/postfix/generic_transport_map -q || (echo "/$aliasuser@$FORCED_SENDING_DOMAIN/    $BOUNCE_MAIL_ADDRESS" >> /etc/postfix/generic_transport_map  ) 
 echo -n ;  }  ;

done
newaliases
postalias /etc/aliases
postmap /etc/postfix/generic_transport_map
postconf -e "mydestination=$MAILNAME"
}

test -e /etc/aliases &&  (
    #postmap lmdb:/etc/aliases;
    postconf -e alias_maps=lmdb:/etc/aliases
    postconf -e alias_database=lmdb:/etc/aliases
    echo )

#[[ -z "$MASQUERADED_SENDER_DOMAINS" ]] || { postconf -e "masquerade_domains=$MASQUERADED_DOMAINS" }
[[ "" = "$FORCED_SENDING_DOMAIN" ]] || {

    ## dkim has to be done by us since we will might have ALLOW_EMPTY_SENDER_DOMAINS SET
        test -e /etc/opendkim/keys/$FORCED_SENDING_DOMAIN.private &&  grep "Domain $FORCED_SENDING_DOMAIN" /etc/opendkim/SigningTable || ( 
#              echo setting up dkim for forced domain  $FORCED_SENDING_DOMAIN ;
              echo "Domain $FORCED_SENDING_DOMAIN"  >> /etc/opendkim/SigningTable)  ; 

                mkdir -p /var/run/opendkim
                chown -R opendkim:opendkim /var/run/opendkim
                dkim_socket=$(cat /etc/opendkim/opendkim.conf | egrep ^Socket | awk '{ print $2 }')
                if [ $(echo "$dkim_socket" | cut -d: -f1) == "inet" ]; then
                        dkim_socket=$(echo "$dkim_socket" | cut -d: -f2)
                        dkim_socket="inet:$(echo "$dkim_socket" | cut -d@ -f2):$(echo "$dkim_socket" | cut -d@ -f1)"
                fi
                echo -e "        ...using socket $dkim_socket"

                do_postconf -e "milter_protocol=6"
                do_postconf -e "milter_default_action=accept"
                do_postconf -e "smtpd_milters=$dkim_socket"
                do_postconf -e "non_smtpd_milters=$dkim_socket"

                echo > /etc/opendkim/TrustedHosts
                echo > /etc/opendkim/KeyTable
                echo > /etc/opendkim/SigningTable

                # Since it's an internal service anyways, it's safe
                # to assume that *all* hosts are trusted.
                echo "0.0.0.0/0" > /etc/opendkim/TrustedHosts
                        for domain in $FORCED_SENDING_DOMAIN; do
                                private_key=/etc/opendkim/keys/${domain}.private
                                if [ -f $private_key ]; then
                                echo -n "privkey_found "
                                        domain_dkim_selector="$(get_dkim_selector "${domain}")"
                                        [[ "$domain_dkim_selector" = "" ]] && domain_dkim_selector=mail
                                        echo -e "        ...for domain ${emphasis}${domain}${reset} (selector: ${emphasis}${domain_dkim_selector}${reset})"
                                        if ! su opendkim -s /bin/bash -c "cat /etc/opendkim/keys/${domain}.private" > /dev/null 2>&1; then
                                                echo -e "        ...trying to reown ${emphasis}${private_key}${reset} as it's not readable by OpenDKIM..."
                                                # Fixes #39
                                                chown opendkim:opendkim "${private_key}"
                                                chmod u+r "${private_key}"
                                        fi

                                        echo "${domain_dkim_selector}._domainkey.${domain} ${domain}:${domain_dkim_selector}:${private_key}" >> /etc/opendkim/KeyTable
                                        echo "*@${domain} ${domain_dkim_selector}._domainkey.${domain}" >> /etc/opendkim/SigningTable

                                else
                                echo no_privkey
                                        error "Skipping DKIM for domain ${emphasis}${domain}${reset}. File ${private_key} not found!"
                                fi
                        done
                        echo signtable:
                        cat /etc/opendkim/SigningTable
                        echo keytable:
                        cat /etc/opendkim/KeyTable

### sender canonical mappings
haveit=no
test -e /etc/postfix/sender_canonical_maps && grep '\.AT\.' /etc/postfix/sender_canonical_maps && grep '@'$FORCED_SENDING_DOMAIN  /etc/postfix/sender_canonical_maps && haveit=yes
[[ "$haveit" = "no" ]]  && {
    ( echo "### sender canonical mappings - MAILNAME PASSTHROUGH"
    echo '/(.+)@'$MAILNAME'/                     $1@'$MAILNAME              ) >  /etc/postfix/sender_canonical_maps
    ( echo "### sender canonical mappings - MAILNAME EMPTY SENDERS e.g. BOUNCE"
    echo '/^$/                        MAILER-DAEMON@'$MAILNAME              
    echo '/^<>$/                      MAILER-DAEMON@'$MAILNAME       ) >>  /etc/postfix/sender_canonical_maps
[[ -z "$FORCED_SENDING_DOMAIN" ]] || {
    [[ "$MAILNAME" = "$FORCED_SENDING_DOMAIN" ]] || { 
    ( echo "### sender canonical mappings - FORCED_SENDING_DOMAIN PASSTHROUGH when DOMAIN  = FORCED_SENDING_DOMAIN  "
    echo '/(.+)@'$FORCED_SENDING_DOMAIN'/        $1@'$FORCED_SENDING_DOMAIN ) >> /etc/postfix/sender_canonical_maps ; } ;
    ( echo "### sender canonical mappings - FORCED_SENDING_DOMAIN PASSTHROUGH when DOMAIN != FORCED_SENDING_DOMAIN  "
    echo '/(.+)@(.+)/                      $1.AT.$2@'$FORCED_SENDING_DOMAIN ) >> /etc/postfix/sender_canonical_maps

echo -n ;  }  ;        


echo -n ;  }  ;

#postmap /etc/postfix/sender_canonical_maps
haveit=no

## 
test -e /etc/postfix/header_checks && grep REPLACE /etc/postfix/header_checks && grep '@'$FORCED_SENDING_DOMAIN  /etc/postfix/header_checks && haveit=yes
[[ "$haveit" = "yes" ]] && echo "FORCED_SENDING_DOMAIN already setup"

[[ "$haveit" = "no" ]] && {
 echo "FORCED_SENDIND_DOMAIN SETUP LAUNCHING"
## dummy rule first so we hit this rule early
( echo '# keep: yourself' 
echo '/^From:(.+)@'$FORCED_SENDING_DOMAIN'([,>])$/ REPLACE From: $1@'$FORCED_SENDING_DOMAIN'$2')  >> /etc/postfix/header_checks
   [[  "$MAILNAME" = "$FORCED_SENDING_DOMAIN" ]] ||  {
grep -qF '/^From:(.+)@'$MAILNAME'([,>])/ REPLACE From: $1@'$MAILNAME'$2' /etc/postfix/header_checks || (
       ( echo "## if mailname and forced_domain differ, rewrite"
       echo '/^From:(.+)@'$MAILNAME'([,>])/ REPLACE From: $1@'$MAILNAME'$2' ) >> /etc/postfix/header_checks )
   }

   [[ "$ALLOW_FORCED_SUBDOMAINS" = "true"  ]] && {
    grep -qF '/^From:(.+)([-_a-zA-Z0-9.+!%]*)@([-_a-zA-Z0-9.]*).'$FORCED_SENDING_DOMAIN'([,>])/ REPLACE From:$1$2@$3.'$FORCED_SENDING_DOMAIN'$4'  /etc/postfix/header_checks || (
       ( echo "## subdomains are eligible ?"
       echo '/^From:(.+)([-_a-zA-Z0-9.+!%]*)@([-_a-zA-Z0-9.]*).'$FORCED_SENDING_DOMAIN'([,>])/ REPLACE From:$1$2@$3.'$FORCED_SENDING_DOMAIN'$4' )  >> /etc/postfix/header_checks )
   }

#  echo '/^From:(.+)@(.+)/ REPLACE From: $1.AT.$2@'$FORCED_SENDING_DOMAIN' ' >> /etc/postfix/header_checks
grep -qF '/^From:(.+)([-_a-zA-Z0-9.+!%]*)@([-_a-zA-Z0-9.]*)([,>])/ REPLACE From:$1$2.AT.$3@'$FORCED_SENDING_DOMAIN'$4' /etc/postfix/header_checks || (
   ( echo "#CATCHALL_RULE"
   echo '/^From:(.+)([-_a-zA-Z0-9.+!%]*)@([-_a-zA-Z0-9.]*)([,>])/ REPLACE From:$1$2.AT.$3@'$FORCED_SENDING_DOMAIN'$4' )>> /etc/postfix/header_checks )
echo -n ;  }  ;

#postmap /etc/postfix/header_checks

postconf -e "sender_canonical_classes = envelope_sender, header_sender"
postconf -e "sender_canonical_maps =  regexp:/etc/postfix/sender_canonical_maps"



     } ## // end FORCED_SENDING_DOMAIN



grep 'From:([,<])([,>])./ REPLACE From: MAILER-DAEMON@'$MAILNAME /etc/postfix/header_checks  || {
  [[ "" = "$FORCED_SENDING_DOMAIN" ]] || {
  grep -qF '/^From:([,<])([,>])$/ '"REPLACE From: MAILER-DAEMON@$FORCED_SENDING_DOMAIN" /etc/postfix/header_checks ||   (
    
    (
      echo "## no empty from FORCED_SENDING_DOMAIN  is set"
      echo '/^From:([,<])([,>])$/ REPLACE From: MAILER-DAEMON@'$FORCED_SENDING_DOMAIN ) >> /etc/postfix/header_checks    )       
  }        
  [[ "" = "$FORCED_SENDING_DOMAIN" ]] && {
  grep -qF '/^From:([,<])([,>])$/ REPLACE From: MAILER-DAEMON@'$MAILNAME /etc/postfix/header_checks ||  (
    (
      echo "## no empty from FORCED_SENDING_DOMAIN NOT set"
      echo '/^From:([,<])([,>])$/ REPLACE From: MAILER-DAEMON@'$MAILNAME ) >> /etc/postfix/header_checks  )        
  }        
}  


grep 'From:(.-_a-zA-Z0-9.+'  /etc/postfix/header_checks  || {
  [[ "" = "$FORCED_SENDING_DOMAIN" ]] || {
  grep -qF '/^From:([-_a-zA-Z0-9.+!%[:space:]]*)$/  REPLACE From: $1'@$FORCED_SENDING_DOMAIN /etc/postfix/header_checks   || (
    (
    echo "## no "just text" From that has no @ - FORCED_SENDING_DOMAIN  is set"
    echo '/^From:([-_a-zA-Z0-9.+!%[:space:]]*)$/  REPLACE From: $1'@$FORCED_SENDING_DOMAIN ) >> /etc/postfix/header_checks  )
  } 
  [[ "" = "$FORCED_SENDING_DOMAIN" ]] && {
  grep -qF '/^From:([-_a-zA-Z0-9.+!%[:space:]]*)$/  REPLACE From: $1'@$MAILNAME /etc/postfix/header_checks   || (
    (
    echo "## no "just text" From that has no @ - FORCED_SENDING_DOMAIN  NOT set"
    echo '/^From:([-_a-zA-Z0-9.+!%[:space:]]*)$/  REPLACE From: $1'@$MAILNAME )>> /etc/postfix/header_checks    )      
  }
}

##grep '<>/ REPLACE From: ' /etc/postfix/header_checks || {
##  [[ "" = "$FORCED_SENDING_DOMAIN" ]] || {
##    echo '/^From:(.+)<>/ REPLACE From: $1 <EMPTY-SENDER@'$FORCED_SENDING_DOMAIN'>' >> /etc/postfix/header_checks
##  }
##  [[ "" = "$FORCED_SENDING_DOMAIN" ]] && {
##    echo '/^From:(.+)<>/ REPLACE From: $1 <EMPTY-SENDER@'$MAILNAME'>' >> /etc/postfix/header_checks
##  }
##}

### snippet:
#x#/^From:(.+)@mail.domain.lan([,>])/ REPLACE From: $1@mail.domain.lan$2
#x#/^From:(.+)([-_a-zA-Z0-9.+!%]*)@([-_a-zA-Z0-9.]*).domain.lan([,>])/ REPLACE From:$1$2@$3.domain.lan$4
#x#/^From:(.+)([-_a-zA-Z0-9.+!%]*)@([-_a-zA-Z0-9.]*)([,>])/ REPLACE From:$1$2.AT.$3@mail.domain.lan$4
#X#/^From:(.+)<>/ REPLACE From: $1 <EMPTY-SENDER@mail.domain.lan>
#X#/^From:([-_a-zA-Z0-9.+!%[:space:]]*)$/  REPLACE From: $1@mail.domain.lan
##
postconf -e "smtp_header_checks = regexp:/etc/postfix/header_checks"
postconf -e "lmtp_header_checks = regexp:/etc/postfix/header_checks"  
postconf -e "header_checks = regexp:/etc/postfix/header_checks"

while (true);do 

if [ ! -d /etc/opendkim/keys ]; then
    postconf -e "non_smtpd_milters ="
    postconf -e "smtpd_milters ="
elif [ -z "$(find /etc/opendkim/keys -type f ! -name .)" ]; then
    postconf -e "non_smtpd_milters ="
    postconf -e "smtpd_milters ="

else
echo -n > /dev/null
fi
sleep 666
done &

#postfix as client
postconf -e "smtp_use_tls = yes" "smtp_tls_security_level = encrypt" "smtp_tls_note_starttls_offer = yes" "smtp_sasl_auth_enable = yes" "smtp_sasl_security_options = noanonymous"
#postfix as server
postconf -e smtpd_sasl_security_options=noanonymous,noplaintext smtpd_sasl_tls_security_options=noanonymous

have_smtp_cred="no"
[[ -z "$SMTP_PASSWORD" ]] || [[ -z "$SMTP_DOMAIN" ]] || [[ -z "$SMTP_DOMAIN" ]] || have_smtp_cred="yes"

[[  "$have_smtp_cred" = "yes" ]] && { 
#### set up sasl password auth
    test -e /etc/sasl2||mkdir /etc/sasl2;(echo "pwcheck_method: auxprop" ;echo "auxprop_plugin: sasldb" ;echo "mech_list: PLAIN LOGIN CRAM-MD5 DIGEST-MD5 NTLM") >/etc/sasl2/smtpd.conf
    postconf -e smtpd_sasl_auth_enable=yes broken_sasl_auth_clients=yes smtpd_sasl_security_options=noanonymous,noplaintext smtpd_sasl_tls_security_options=noanonymous smtpd_relay_restrictions=permit_sasl_authenticated,reject_unauth_destination
    postconf -e "cyrus_sasl_config_path = /etc/sasl2, /etc/postfix/sasl/, /var/lib/sasl2"
    echo "###SASL:SPAWN##"    
#    while (true);do /usr/sbin/saslauthd -a sasldb -c -d ;sleep 5;done &
    (echo '#!/bin/sh'; 
    echo "ps -A|grep -v grep |grep saslauthd && killall -QUIT saslauthd"
    echo "/usr/sbin/saslauthd -a sasldb -c -d 2>&1  |sed  's/^/saslauthd: /g'|grep -e realm= -e service= -e response:" )> /scripts/saslstart.sh
    chmod +x /scripts/saslstart.sh
    grep saslauth /etc/supervisord.conf || (echo '[program:saslauthd]
command         = /scripts/saslstart.sh
autostart       = true
autorestart     = true
startsecs       = 5
stopwaitsecs    = 5
stdout_logfile  = /dev/stdout
stderr_logfile  = /dev/stderr
stdout_logfile_maxbytes = 0
stderr_logfile_maxbytes = 0' >> /etc/supervisord.conf )
    echo "ADDING SMTP USER ${SMTP_USER}@${SMTP_DOMAIN}"
    echo "${SMTP_PASSWORD}" | saslpasswd2 -p -c -u "${SMTP_DOMAIN}" "${SMTP_USER}"
    ( echo '#!/bin/bash' ; echo ' read psw;echo "$psw" | saslpasswd2 -p -c -u ${1/*@/} ${1/@*/}') > /usr/bin/add_smtp_user
    chmod +x /usr/bin/add_smtp_user
    [[ -z "$SMTP_MORE_USERS" ]] || (
        #SMTP_MORE_USERS has to be like "username@domain.tld:USERsPASSWORD1273123 anottherone@sampledomain.lan:OIQ§O$IJQ§OIJQ§O$IJQ§O$IJQ§$"
        echo "ADDING_SMTP_CLIENT_USERS.."
        echo "$SMTP_MORE_USERS"|sed 's/ /\n/'|grep @|while read curline;do 
          myuser=${curline/:*/}
          mypass=${curline/*:/}
          echo "ADDING SMTP USER $myuser"
          echo "$mypass" | add_smtp_user $myuser
        done
    )
    chgrp postfix /etc/sasl2/sasldb2 /etc/sasl2 
    chmod g+r /etc/sasl2/sasldb2 /etc/sasl2 
    ( sleep 30 ; 
    echo "###SASL:TEST##"   ;  
    testsaslauthd -u "${SMTP_USER}" -r "${SMTP_DOMAIN}" -p "${SMTP_PASSWORD}" -s smtpd ) &

    echo   ; } ;
echo "###ALIAS:DONE##"    

grep "supervisorctl" /etc/supervisord.conf || {
echo '[unix_http_server]
file = /tmp/supervisor.sock
chmod = 0777
chown= nobody:nogroup
username = supervisor
password = InternalControl
#[inet_http_server]
#port = 127.0.0.1:9001
#username = user
#password = 123
[supervisorctl]
serverurl = unix:///tmp/supervisor.sock
username = supervisor
password = InternalControl
prompt = sprvsrctl

[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface
'  >> /etc/supervisord.conf ; } ;
 echo "## end supervisorctl "
#exit 0
