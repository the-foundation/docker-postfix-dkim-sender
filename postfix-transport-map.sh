#!/bin/bash 


apk del  postfix-pcre
apk add  postfix-pcre
#postmap /etc/postfix/transport

postconf -p |grep smtpd_recipient_restrictions |grep "= reject_non_fqdn_recipient, reject_unknown_recipient_domain, check_sender_access lmdb:/etc/postfix/allowed_senders, reject" && { 
    postconf -e "smtpd_recipient_restrictions = permit_mynetworks,reject_non_fqdn_recipient, reject_unknown_recipient_domain, check_sender_access lmdb:/etc/postfix/allowed_senders, reject"
}
postconf -e "transport_maps=pcre:/etc/postfix/transport_maps_pcre"
postconf -e "smtp_generic_maps=pcre:/etc/postfix/generic_transport_map" 

# SMTPD TLS configuration for incoming connections
mkdir -p /etc/pki/tls/private/ /etc/pki/tls/certs/ &
cd /tmp/

openssl req -new -x509 -days 1 -nodes -newkey rsa:4096 -keyout private.key -out public.cert -subj "/C=US/ST=Ca/L=Sunnydale/CN=$(hostname -f)"

mv private.key /etc/pki/tls/private/
mv public.cert /etc/pki/tls/certs/

postconf -e "smtpd_use_tls = yes"
postconf -e "smtpd_tls_cert_file = /etc/pki/tls/certs/public.cert"
postconf -e "smtpd_tls_key_file = /etc/pki/tls/private/private.key"
postconf -e "smtpd_tls_security_level = may"
postconf -e "smtp_sasl_password_maps = lmdb:/etc/postfix/sasl_passwd"
postmap lmdb:/etc/postfix/sasl_passwd
echo "###TRANSPORT:DONE##"    

echo "$RELAYHOST"|grep randmap && {
postconf -e "relayhost="
postconf -e "transport_maps=$RELAYHOST"
echo -n ; } ;

echo "###RELAYHOST:DONE##"    

#exit 0

